import pandas as pd
from data_ingest import mass_shooting_data, mass_murder_data
import re
import os
import geopy
from geopy.geocoders import Nominatim
geolocator = Nominatim(user_agent="mass_shootings")

##Merge Dataframes & remove duplicates
mass_murder_data_frame = pd.concat(mass_murder_data, sort=False, axis=0)
mm_df = mass_murder_data_frame.drop_duplicates(keep=False).fillna(0)


def age_variable(row):
    if row['Perpetrator'] != 0:
        x = re.findall(r'[0-9]{2}', row['Perpetrator'])
    elif row['Perpetrator'] == 0:
        x = re.findall(r'[0-9]{2}', row['Name'])
    else:
        x == 0
    if len(x) > 0:
        return x
    else:
        return None


def name_variable(row):
    if row['Perpetrator'] != 0:
        return ' '.join(re.findall(r'[A-Z][a-z]{1,}', row['Perpetrator']))
    elif row['Perpetrator'] == 0:
        return ' '.join(re.findall(r'[A-Z][a-z]{1,}', row['Name']))
    else:
        return None


def country_name(row):
    states_list = [
        'Alabama',
        'Alaska',
        'Arizona',
        'Arkansas',
        'California',
        'Colorado',
        'Connecticut',
        'Delaware',
        'Florida',
        'Georgia',
        'Hawaii',
        'Idaho',
        'Illinois',
        'Indiana',
        'Iowa',
        'Kansas',
        'Kentucky',
        'Louisiana',
        'Maine',
        'Maryland',
        'Massachusetts',
        'Michigan',
        'Minnesota',
        'Mississippi',
        'Missouri',
        'Montana',
        'Nebraska',
        'Nevada',
        'New Hampshire',
        'New Jersey',
        'New Mexico',
        'New York',
        'North Carolina',
        'North Dakota',
        'Ohio',
        'Oklahoma',
        'Oregon',
        'Pennsylvania',
        'Rhode Island',
        'South Carolina',
        'South Dakota',
        'Tennessee',
        'Texas',
        'Utah',
        'Vermont',
        'Virginia',
        'Washington',
        'Washington, D.C.',
        'West Virginia',
        'Wyoming',
        'Wisconsin',
    ]
    if row['Country'] != 0:
        if row['State'] in (states_list):
            return 'U.S.'
        else:
            return row['Country']
        return row['Country']
    elif row['State'] != 0:
        if row['State'] in (states_list):
            return 'U.S.'
        else:
            return row['State']
    elif row['Location'] != 0:
        return 'China'


clean_data = mm_df.dropna()
clean_data['Age'] = clean_data.apply(lambda row: age_variable(row), axis=1)
clean_data['Full_Name'] = clean_data.apply(
    lambda row: name_variable(row), axis=1)
clean_data['Geo'] = clean_data.apply(lambda row: country_name(row), axis=1)
clean_data['Month'] = clean_data['Date'].apply(lambda x: str(x)[:3])
filled_df = clean_data[[
    'Full_Name', 'Month', 'Age', 'Year', 'Geo', 'W', 'Killed', 'Injured'
]].fillna(0)

# filled_df['W'] = filled_df['W'].apply(lambda x: recode_w(x))
weapon_dummy_df = filled_df.W.str.join('|').str.get_dummies().rename(
    columns={
        'F': 'firearm',
        'M': 'melee',
        'O': 'other',
        'V': 'vehicule',
        'A': 'arson',
        'E': 'explosives',
        'P': 'gases'
    })

print weapon_dummy_df.tail(n=10)