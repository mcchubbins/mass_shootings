import os
import pandas as pd
import re


class CollectItems:
    def __init__(self, folder_name):
        self.folder_name = folder_name
        self.program_path = os.getcwd()
        self.directory = self.program_path + '/data/' + folder_name
        self.data_files = os.listdir(self.directory)
        self.data_files_paths = [
            self.directory + '/' + x for x in self.data_files
        ]

    def amass_data(self):
        self.dataframes = []
        for i in self.data_files_paths:
            self.dataframes.append(pd.read_csv(i, sep=','))
        return self.dataframes


mass_shooting = CollectItems('us-mass-shootings-last-50-years')
mass_shooting_data = mass_shooting.amass_data()[4]

mass_murder = CollectItems('data_rampage')
mass_murder_data = mass_murder.amass_data()


def simple_recoding():
    shootings = mass_shooting_data.dropna()
    shootings['Date'] = pd.to_datetime(shootings['Date'])
    shootings['Shooting_Year'] = shootings['Date'].dt.year
    shootings['Shooting_Month'] = shootings['Date'].dt.month

    shootings['Age'] = shootings['Summary'].apply(lambda x: age_gather(x))
    shootings['City'] = shootings['Location'].str.rpartition(',')[
        0]  #.str.replace(",", " ")
    shootings['State'] = shootings['Location'].str.rpartition(',')[2]
    return shootings


def age_gather(x):
    for item in x:
        if '-year' in item:
            age = [
                x.replace('-', '') for x in item.split('-year')[0].split(' ')
                if len(x) < 4 if '-' in x
            ]
        elif 'year-' in item:
            age = [
                x.replace('-', '') for x in item.split('year-')[0].split(' ')
                if len(x) < 4 if '-' in x
            ]
        elif len(re.findall('\,\ [0-9]{2}\,', item)) == 1:
            age = re.findall('\,\ [0-9]{2}\,', item)[0]
        elif len(re.findall('\,\ [0-9]{2}\,', item)) > 1:
            age = str([
                x.replace(' ', '').replace(',', '')
                for x in re.findall('\,\ [0-9]{2}\,', item)
            ])
        elif len(re.findall('\,\ [0-9]{2}\,', item)) == 0:
            age = 'no age provided'
        else:
            age = 'no age provided'
    return age
